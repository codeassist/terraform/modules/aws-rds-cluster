# ----------------
# Common settings
# ----------------
variable "rds_cluster_module_enabled" {
  description = "Whether to create the resources."
  type        = bool
  # `false` prevents the module from creating any resources
  default = false
}

# -------------------
# General parameters
# -------------------
variable "rds_cluster_identifier" {
  # If omitted, Terraform will assign a random, unique identifier.
  description = "The cluster identifier."
  type        = string
}

variable "rds_cluster_tags" {
  description = "A mapping of tags to assign to the RDS resources."
  type        = map(string)
  default     = {}
}

variable "rds_cluster_size" {
  description = "Number of DB instances to create in the cluster."
  type        = number
  default     = 1
}
variable "rds_cluster_instance_class" {
  description = "Instance type to use."
  type        = string
  default     = "db.t3.small"
}
variable "rds_cluster_publicly_accessible" {
  description = "Set to true if you want your cluster to be publicly accessible (such as via QuickSight)."
  type        = bool
  default     = false
}


# --------------------------
# Network/Security settings
# --------------------------
variable "rds_cluster_vpc_id" {
  description = "VPC ID to create the cluster in."
  type        = string
}

variable "rds_cluster_vpc_subnets" {
  description = "List of VPC subnet IDs."
  type        = list(string)
}

variable "rds_cluster_allowed_security_groups_count" {
  description = "To prevent an error `count depends` or `count can not be computed`."
  type        = number
  default     = 0
}

variable "rds_cluster_allowed_security_groups" {
  description = "List of security groups to be allowed to connect to the DB instance."
  type        = list(string)
  default     = []
}

variable "rds_cluster_allowed_cidr_blocks" {
  description = "List of CIDR blocks allowed to access the cluster."
  type        = list(string)
  default     = []
}

variable "rds_cluster_db_port" {
  description = "Database port."
  type        = number
  default     = 3306
}


# ---------------------
# RDS cluster resource
# ---------------------
variable "rds_cluster_family" {
  description = "The family of the DB cluster parameter group."
  type        = string
  # To list all of the available parameter group families, use the following command:
  #   aws rds describe-db-engine-versions --query "DBEngineVersions[].DBParameterGroupFamily"
  default = "aurora-mysql5.7"
}
variable "rds_cluster_parameters" {
  description = "List of DB cluster parameters to apply."
  type = list(object({
    apply_method = string
    name         = string
    value        = string
  }))
  default = []
}
variable "rds_cluster_instance_parameters" {
  description = "List of DB instance parameters to apply."
  type = list(object({
    apply_method = string
    name         = string
    value        = string
  }))
  default = []
}

variable "rds_cluster_db_name" {
  description = "Name for an automatically created database on cluster creation."
  # There are different naming restrictions per database engine: RDS Naming Constraints:
  #   * http://docs.aws.amazon.com/AmazonRDS/latest/UserGuide/CHAP_Limits.html#RDS_Limits.Constraints
  type    = string
  default = ""
}
variable "rds_cluster_deletion_protection" {
  description = "If the DB instance should have deletion protection enabled."
  type        = bool
  default     = false
}
variable "rds_cluster_master_username" {
  description = "(Required unless a snapshot_identifier is provided) Username for the master DB user."
  type        = string
  default     = "admin"
}
variable "rds_cluster_master_password" {
  description = "(Required unless a snapshot_identifier is provided) Password for the master DB user."
  type        = string
  default     = ""
}
variable "rds_cluster_skip_final_snapshot" {
  description = "Determines whether a final DB snapshot is created before the DB cluster is deleted."
  type        = bool
  default     = true
}
variable "rds_cluster_backtrack_window" {
  description = "The target backtrack window, in seconds. Only available for aurora engine currently. Must be between 0 and 259200 (72 hours)."
  type        = number
  default     = 0
}
variable "rds_cluster_retention_period" {
  description = "Number of days to retain backups for."
  type        = number
  default     = 7
}
variable "rds_cluster_preferred_backup_window" {
  description = "Daily time range during which the backups happen."
  type        = string
  # Time in UTC.
  # Default: a 30-minute window selected at random from an 8-hour block of time per region.
  default = "01:00-04:00"
}
variable "rds_cluster_preferred_maintenance_window" {
  description = "Weekly time range during which system maintenance can occur, in UTC."
  type        = string
  default     = "sun:00:00-sun:01:00"
}
variable "rds_cluster_snapshot_identifier" {
  description = "Specifies whether or not to create this cluster from a snapshot."
  type        = string
  default     = null
}
variable "rds_cluster_global_identifier" {
  description = "(Optional) The global cluster identifier specified on `aws_rds_global_cluster`."
  # https://www.terraform.io/docs/providers/aws/r/rds_global_cluster.html
  type    = string
  default = null
}
variable "rds_cluster_replication_source_identifier" {
  description = "ARN of a source DB cluster or DB instance if this DB cluster is to be created as a Read Replica."
  type        = string
  default     = null
}
variable "rds_cluster_storage_encrypted" {
  description = "Specifies whether the DB cluster is encrypted. The default is `false` for `provisioned` `engine_mode` and `true` for `serverless` `engine_mode`."
  type        = bool
  default     = false
}
variable "rds_cluster_kms_key_id" {
  description = "The ARN for the KMS encryption key. When specifying `kms_key_arn`, `storage_encrypted` needs to be set to `true`."
  type        = string
  default     = null
}
variable "rds_cluster_apply_immediately" {
  description = "Specifies whether any cluster modifications are applied immediately, or during the next maintenance window."
  type        = bool
  default     = true
}
variable "rds_cluster_iam_roles" {
  description = "IAM roles for the Aurora cluster."
  type        = list(string)
  default     = []
}
variable "rds_cluster_iam_database_authentication_enabled" {
  description = "Specifies whether or mappings of AWS Identity and Access Management (IAM) accounts to database accounts is enabled."
  type        = bool
  default     = false
}
variable "rds_cluster_engine" {
  description = "The name of the database engine to be used for this DB cluster."
  type        = string
  # Valid Values:
  #   aurora (for MySQL 5.6-compatible Aurora)
  #   aurora-mysql (for MySQL 5.7-compatible Aurora)
  #   aurora-postgresql
  default = "aurora-mysql"
}
variable "rds_cluster_engine_mode" {
  description = "The database engine mode. Valid values: `global`, `multimaster`, `parallelquery`, `provisioned`, `serverless`."
  type        = string
  default     = "provisioned"
}
variable "rds_cluster_engine_version" {
  description = "The version number of the database engine to use."
  type        = string
  default     = null
}
variable "rds_cluster_source_region" {
  description = "Source Region of primary cluster, needed when using encrypted storage and region replicas."
  type        = string
  default     = null
}
variable "rds_cluster_enabled_cloudwatch_logs_exports" {
  description = "List of log types to export to cloudwatch. The following log types are supported: audit, error, general, slowquery, postgresql (PostgreSQL)."
  type        = list(string)
  default     = []
}
variable "rds_cluster_scaling_configuration" {
  description = "List of nested attributes with scaling properties. Only valid when `engine_mode` is set to `serverless`."
  type = list(object({
    auto_pause               = bool
    max_capacity             = number
    min_capacity             = number
    seconds_until_auto_pause = number
  }))
  default = []
}

# ----------------------------
# Instance of the RDS cluster
# ----------------------------
variable "rds_cluster_monitoring_interval" {
  description = "Interval in seconds that metrics are collected, 0 to disable (values can only be 0, 1, 5, 10, 15, 30, 60)."
  type        = number
  default     = 0
}
variable "rds_cluster_availability_zone" {
  description = "Optional parameter to place cluster instances in a specific availability zone. If left empty, will place randomly."
  type        = string
  default     = null
}
variable "rds_cluster_performance_insights_enabled" {
  description = "Whether to enable Performance Insights."
  type        = bool
  default     = false
}
variable "rds_cluster_performance_insights_kms_key_id" {
  description = "The ARN for the KMS key to encrypt Performance Insights data. When specifying `performance_insights_kms_key_id`, `performance_insights_enabled` needs to be set to true."
  type        = string
  default     = null
}


# ------------------------
# RDS cluster autoscaling
# ------------------------
variable "rds_cluster_autoscaling_enabled" {
  description = "Whether to enable cluster autoscaling."
  type        = bool
  default     = false
}
variable "rds_cluster_autoscaling_min_capacity" {
  description = "Minimum number of instances to be maintained by the autoscaler."
  type        = number
  default     = 1
}
variable "rds_cluster_autoscaling_max_capacity" {
  description = "Maximum number of instances to be maintained by the autoscaler."
  type        = number
  default     = 5
}
variable "rds_cluster_autoscaling_policy_type" {
  description = "Autoscaling policy type. `TargetTrackingScaling` and `StepScaling` are supported."
  type        = string
  default     = "TargetTrackingScaling"
}
variable "rds_cluster_autoscaling_target_value" {
  description = "The target value to scale with respect to target metrics."
  type        = number
  default     = 75
}
variable "rds_cluster_autoscaling_scale_in_cooldown" {
  description = "The amount of time, in seconds, after a scaling activity completes and before the next scaling down activity can start. Default is 300s."
  type        = number
  default     = 300
}
variable "rds_cluster_autoscaling_scale_out_cooldown" {
  description = "The amount of time, in seconds, after a scaling activity completes and before the next scaling up activity can start. Default is 300s."
  type        = number
  default     = 300
}
variable "rds_cluster_autoscaling_predefined_metric_type" {
  description = "The metric type to use. If this value isn't provided the default is CPU utilization."
  type        = string
  default     = "RDSReaderAverageCPUUtilization"
}
