# https://docs.aws.amazon.com/AmazonRDS/latest/APIReference/API_CreateDBClusterParameterGroup.html

provider "aws" {
  region = "us-west-1"

  # Make it faster by skipping some checks
  skip_get_ec2_platforms      = true
  skip_metadata_api_check     = true
  skip_region_validation      = true
  skip_credentials_validation = true
  skip_requesting_account_id  = true
}

module "rds_cluster_aurora_mysql" {
  source          = "../../"
  rds_cluster_engine          = "aurora"
  rds_cluster_family  = "aurora-mysql5.7"
  rds_cluster_size    = "2"
  namespace       = "eg"
  stage           = "dev"
  name            = "db"
  rds_cluster_master_username      = "admin1"
  rds_cluster_master_password  = "Test123456789"
  rds_cluster_db_name         = "dbname"
  rds_cluster_instance_class   = "db.t2.small"
  rds_cluster_vpc_id          = "vpc-xxxxxxx"
  rds_cluster_allowed_security_groups = ["sg-xxxxxxxx"]
  rds_cluster_vpc_subnets         = ["subnet-xxxxxxxx", "subnet-xxxxxxxx"]
  zone_id         = "Zxxxxxxxx"

  rds_cluster_parameters = [
    {
      name         = "character_set_client"
      value        = "utf8"
      apply_method = "pending-reboot"
    },
    {
      name         = "character_set_connection"
      value        = "utf8"
      apply_method = "pending-reboot"
    },
    {
      name         = "character_set_database"
      value        = "utf8"
      apply_method = "pending-reboot"
    },
    {
      name         = "character_set_results"
      value        = "utf8"
      apply_method = "pending-reboot"
    },
    {
      name         = "character_set_server"
      value        = "utf8"
      apply_method = "pending-reboot"
    },
    {
      name         = "collation_connection"
      value        = "utf8_bin"
      apply_method = "pending-reboot"
    },
    {
      name         = "collation_server"
      value        = "utf8_bin"
      apply_method = "pending-reboot"
    },
    {
      name         = "lower_case_table_names"
      value        = "1"
      apply_method = "pending-reboot"
    },
    {
      name         = "skip-character-set-client-handshake"
      value        = "1"
      apply_method = "pending-reboot"
    }
  ]
}
