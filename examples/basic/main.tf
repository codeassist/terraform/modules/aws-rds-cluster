# https://docs.aws.amazon.com/AmazonRDS/latest/APIReference/API_CreateDBClusterParameterGroup.html

provider "aws" {
  region = "us-west-1"

  # Make it faster by skipping some checks
  skip_get_ec2_platforms      = true
  skip_metadata_api_check     = true
  skip_region_validation      = true
  skip_credentials_validation = true
  skip_requesting_account_id  = true
}

module "rds_cluster_aurora_postgres" {
  source          = "../../"
  name            = "postgres"
  rds_cluster_engine          = "aurora-postgresql"
  rds_cluster_family  = "aurora-postgresql9.6"
  rds_cluster_size    = "2"
  namespace       = "eg"
  stage           = "dev"
  rds_cluster_master_username      = "admin1"
  rds_cluster_master_password  = "Test123456789"
  rds_cluster_db_name         = "dbname"
  rds_cluster_db_port         = "5432"
  rds_cluster_instance_class   = "db.r4.large"
  rds_cluster_vpc_id          = "vpc-xxxxxxxx"
  rds_cluster_allowed_security_groups = ["sg-xxxxxxxx"]
  rds_cluster_vpc_subnets         = ["subnet-xxxxxxxx", "subnet-xxxxxxxx"]
  zone_id         = "Zxxxxxxxx"
}
