# https://docs.aws.amazon.com/AmazonRDS/latest/APIReference/API_CreateDBClusterParameterGroup.html
# https://docs.aws.amazon.com/AmazonRDS/latest/AuroraUserGuide/AuroraMySQL.Updates.20180206.html
# https://docs.aws.amazon.com/AmazonRDS/latest/AuroraUserGuide/aurora-serverless.html

provider "aws" {
  region = "us-west-1"

  # Make it faster by skipping some checks
  skip_get_ec2_platforms      = true
  skip_metadata_api_check     = true
  skip_region_validation      = true
  skip_credentials_validation = true
  skip_requesting_account_id  = true
}

module "rds_cluster_aurora_mysql_serverless" {
  source          = "../../"
  namespace       = "eg"
  stage           = "dev"
  name            = "db"
  rds_cluster_engine          = "aurora"
  rds_cluster_engine_mode     = "serverless"
  rds_cluster_family  = "aurora5.6"
  rds_cluster_size    = "0"
  rds_cluster_master_username      = "admin1"
  rds_cluster_master_password  = "Test123456789"
  rds_cluster_db_name         = "dbname"
  rds_cluster_db_port         = "3306"
  rds_cluster_instance_class   = "db.t2.small"
  rds_cluster_vpc_id          = "vpc-xxxxxxxx"
  rds_cluster_allowed_security_groups = ["sg-xxxxxxxx"]
  rds_cluster_vpc_subnets         = ["subnet-xxxxxxxx", "subnet-xxxxxxxx"]
  zone_id         = "Zxxxxxxxx"

  rds_cluster_scaling_configuration = [
    {
      auto_pause               = true
      max_capacity             = 256
      min_capacity             = 2
      seconds_until_auto_pause = 300
    }
  ]
}
