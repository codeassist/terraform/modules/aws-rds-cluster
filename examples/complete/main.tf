provider "aws" {
  region = var.region
}

module "vpc" {
  source     = "git::https://github.com/cloudposse/terraform-aws-vpc.git?ref=tags/0.7.0"
  namespace  = var.namespace
  stage      = var.stage
  name       = var.name
  cidr_block = "172.16.0.0/16"
}

module "subnets" {
  source               = "git::https://github.com/cloudposse/terraform-aws-dynamic-subnets.git?ref=tags/0.16.0"
  availability_zones   = var.availability_zones
  namespace            = var.namespace
  stage                = var.stage
  name                 = var.name
  vpc_id               = module.vpc.vpc_id
  igw_id               = module.vpc.igw_id
  cidr_block           = module.vpc.vpc_cidr_block
  nat_gateway_enabled  = false
  nat_instance_enabled = false
}

module "rds_cluster" {
  source              = "../../"
  namespace           = var.namespace
  stage               = var.stage
  name                = var.name
  rds_cluster_engine              = var.engine
  rds_cluster_engine_mode         = var.engine_mode
  rds_cluster_family      = var.cluster_family
  rds_cluster_size        = var.cluster_size
  rds_cluster_master_username          = var.admin_user
  rds_cluster_master_password      = var.admin_password
  rds_cluster_db_name             = var.db_name
  rds_cluster_instance_class       = var.instance_type
  rds_cluster_vpc_id              = module.vpc.vpc_id
  rds_cluster_vpc_subnets             = module.subnets.private_subnet_ids
  rds_cluster_allowed_security_groups     = [module.vpc.vpc_default_security_group_id]
  rds_cluster_deletion_protection = var.deletion_protection
  rds_cluster_autoscaling_enabled = var.autoscaling_enabled

  rds_cluster_parameters = [
    {
      name         = "character_set_client"
      value        = "utf8"
      apply_method = "pending-reboot"
    },
    {
      name         = "character_set_connection"
      value        = "utf8"
      apply_method = "pending-reboot"
    },
    {
      name         = "character_set_database"
      value        = "utf8"
      apply_method = "pending-reboot"
    },
    {
      name         = "character_set_results"
      value        = "utf8"
      apply_method = "pending-reboot"
    },
    {
      name         = "character_set_server"
      value        = "utf8"
      apply_method = "pending-reboot"
    },
    {
      name         = "collation_connection"
      value        = "utf8_bin"
      apply_method = "pending-reboot"
    },
    {
      name         = "collation_server"
      value        = "utf8_bin"
      apply_method = "pending-reboot"
    },
    {
      name         = "lower_case_table_names"
      value        = "1"
      apply_method = "pending-reboot"
    },
    {
      name         = "skip-character-set-client-handshake"
      value        = "1"
      apply_method = "pending-reboot"
    }
  ]
}
