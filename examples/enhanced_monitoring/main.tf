# https://docs.aws.amazon.com/AmazonRDS/latest/APIReference/API_CreateDBClusterParameterGroup.html

provider "aws" {
  region = "us-west-1"

  # Make it faster by skipping some checks
  skip_get_ec2_platforms      = true
  skip_metadata_api_check     = true
  skip_region_validation      = true
  skip_credentials_validation = true
  skip_requesting_account_id  = true
}

# create IAM role for monitoring
resource "aws_iam_role" "enhanced_monitoring" {
  name               = "rds-cluster-example-1"
  assume_role_policy = data.aws_iam_policy_document.enhanced_monitoring.json
}

# Attach Amazon's managed policy for RDS enhanced monitoring
resource "aws_iam_role_policy_attachment" "enhanced_monitoring" {
  role       = aws_iam_role.enhanced_monitoring.name
  policy_arn = "arn:aws:iam::aws:policy/service-role/AmazonRDSEnhancedMonitoringRole"
}

# allow rds to assume this role
data "aws_iam_policy_document" "enhanced_monitoring" {
  statement {
    actions = [
      "sts:AssumeRole",
    ]

    effect = "Allow"

    principals {
      type        = "Service"
      identifiers = ["monitoring.rds.amazonaws.com"]
    }
  }
}

module "rds_cluster_aurora_postgres" {
  source          = "../../"
  rds_cluster_engine          = "aurora-postgresql"
  rds_cluster_family  = "aurora-postgresql9.6"
  rds_cluster_size    = "2"
  namespace       = "eg"
  stage           = "dev"
  name            = "db"
  rds_cluster_master_username      = "admin1"
  rds_cluster_master_password  = "Test123456789"
  rds_cluster_db_name         = "dbname"
  rds_cluster_db_port         = "5432"
  rds_cluster_instance_class   = "db.r4.large"
  rds_cluster_vpc_id          = "vpc-xxxxxxx"
  rds_cluster_allowed_security_groups = ["sg-xxxxxxxx"]
  rds_cluster_vpc_subnets         = ["subnet-xxxxxxxx", "subnet-xxxxxxxx"]
  zone_id         = "Zxxxxxxxx"

  # enable monitoring every 30 seconds
  rds_cluster_monitoring_interval = 30

  # reference iam role created above
  rds_cluster_monitoring_role_arn = aws_iam_role.enhanced_monitoring.arn
}
