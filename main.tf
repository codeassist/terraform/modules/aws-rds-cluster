locals {
  enabled = var.rds_cluster_module_enabled ? true : false

  tags = merge(
    var.rds_cluster_tags,
    {
      terraform = "true"
    }
  )

  min_db_instance_count  = var.rds_cluster_autoscaling_enabled ? var.rds_cluster_autoscaling_min_capacity : var.rds_cluster_size
  cluster_instance_count = local.enabled ? local.min_db_instance_count : 0
}


# ---------------------------
# Network/Security resources
# ---------------------------
resource "aws_db_subnet_group" "cluster_subnet_group" {
  # Provides an RDS DB subnet group resource.
  count = local.enabled ? 1 : 0

  # (Optional, Forces new resource) Creates a unique name beginning with the specified prefix. Conflicts with `name`.
  name_prefix = format("%s-subnet-group-", var.rds_cluster_identifier)
  # (Optional) The description of the DB subnet group. Defaults to "Managed by Terraform".
  description = "The group of subnets allowed to place DB instances of the RDS [${var.rds_cluster_identifier}] Cluster."
  # (Optional) A mapping of tags to assign to the resource.
  tags = merge(
    local.tags,
    {
      Name = format("%s-subnet-group", var.rds_cluster_identifier)
    }
  )

  # (Required) A list of VPC subnet IDs.
  subnet_ids = var.rds_cluster_vpc_subnets
}

# NOTE(!): to prevent security group resource fail when changing in-place add lifecycle and change from
# using `name` to `name_prefix`, see:
#   * https://github.com/hashicorp/terraform/issues/8617
resource "aws_security_group" "rds_cluster" {
  # Provides a security group resource.
  count = local.enabled ? 1 : 0

  # (Optional, Forces new resource) Creates a unique name beginning with the specified prefix. Conflicts with `name`.
  name_prefix = format("%s-rds-sg-", var.rds_cluster_identifier)
  # (Optional, Forces new resource) The security group description. Defaults to "Managed by Terraform". Cannot be "".
  description = "The Security Group for RDS [${var.rds_cluster_identifier}] Cluster."
  # (Optional, Forces new resource) The VPC ID.
  vpc_id = var.rds_cluster_vpc_id
  # (Optional) A mapping of tags to assign to the resource.
  tags = merge(
    local.tags,
    {
      Name = format("%s-rds-sg", var.rds_cluster_identifier)
    }
  )

  lifecycle {
    create_before_destroy = true
  }
}
resource "aws_security_group_rule" "allow_all_egress" {
  # Provides a security group rule resource. Represents a single ingress or egress group rule, which can be added to
  # external Security Groups.
  count = local.enabled ? 1 : 0

  # (Required) The type of rule being created. Valid options are ingress (inbound) or egress (outbound).
  type = "egress"
  # (Optional) Description of the rule.
  description = "Allow ALL Egress traffic from the RDS Cluster."
  # (Required) The start port (or ICMP type number if protocol is "icmp").
  from_port = 0
  # (Required) The end port (or ICMP code if protocol is "icmp").
  to_port = 0
  # (Required) The protocol. If not `icmp`, `tcp`, `udp`, or `all` use the protocol number, see at:
  #   * https://www.iana.org/assignments/protocol-numbers/protocol-numbers.xhtml
  protocol = "-1"
  # (Optional) List of CIDR blocks. Cannot be specified with `source_security_group_id`.
  cidr_blocks = ["0.0.0.0/0"]
  # (Required) The security group to apply this rule to.
  security_group_id = join("", aws_security_group.rds_cluster.*.id)
}
resource "aws_security_group_rule" "allow_ingress_cidrs" {
  # Provides a security group rule resource. Represents a single ingress or egress group rule, which can be added to
  # external Security Groups.
  count = (local.enabled && length(compact(var.rds_cluster_allowed_cidr_blocks)) > 0) ? 1 : 0

  # (Required) The type of rule being created. Valid options are ingress (inbound) or egress (outbound).
  type = "ingress"
  # (Optional) Description of the rule.
  description = "Allow ingress TCP connections on the specified port from specified CIDR blocks."
  # (Required) The start port (or ICMP type number if protocol is "icmp").
  from_port = var.rds_cluster_db_port
  # (Required) The end port (or ICMP code if protocol is "icmp").
  to_port = var.rds_cluster_db_port
  # (Required) The protocol. If not `icmp`, `tcp`, `udp`, or `all` use the protocol number, see at:
  #   * https://www.iana.org/assignments/protocol-numbers/protocol-numbers.xhtml
  protocol = "-1"
  # (Optional) List of CIDR blocks. Cannot be specified with `source_security_group_id`.
  cidr_blocks = var.rds_cluster_allowed_cidr_blocks
  # (Required) The security group to apply this rule to.
  security_group_id = join("", aws_security_group.rds_cluster.*.id)
}
resource "aws_security_group_rule" "allow_ingress_security_groups" {
  # Provides a security group rule resource. Represents a single ingress or egress group rule, which can be added to
  # external Security Groups.
  count = local.enabled ? var.rds_cluster_allowed_security_groups_count : 0

  # (Required) The type of rule being created. Valid options are ingress (inbound) or egress (outbound).
  type = "ingress"
  # (Optional) Description of the rule.
  description = "Allow ingress TCP connection on the specified port from the [${var.rds_cluster_allowed_security_groups[count.index]}] Security Group."
  # (Required) The start port (or ICMP type number if protocol is "icmp").
  from_port = var.rds_cluster_db_port
  # (Required) The end port (or ICMP code if protocol is "icmp").
  to_port = var.rds_cluster_db_port
  # (Required) The protocol. If not `icmp`, `tcp`, `udp`, or `all` use the protocol number, see at:
  #   * https://www.iana.org/assignments/protocol-numbers/protocol-numbers.xhtml
  protocol = "-1"
  # (Optional) The security group id to allow access to/from, depending on the type. Cannot be specified with
  # `cidr_blocks` and `self`.
  source_security_group_id = var.rds_cluster_allowed_security_groups[count.index]
  # (Required) The security group to apply this rule to.
  security_group_id = join("", aws_security_group.rds_cluster.*.id)
}


# ------------------------------------------------------------------------
# Setup required IAM resources (policies and role) to be used by ECS Task
# ------------------------------------------------------------------------
data "aws_iam_policy_document" "rds_assume_role" {
  # Generates an IAM policy document in JSON format.
  # This is a data source which can be used to construct a JSON representation of an IAM policy document, for use with
  # resources which expect policy documents, such as the aws_iam_policy resource.
  count = local.enabled ? 1 : 0

  statement {
    sid     = "AllowRDSServiceAssumeARole"
    effect  = "Allow"
    actions = ["sts:AssumeRole"]
    principals {
      type        = "Service"
      identifiers = ["monitoring.rds.amazonaws.com"]
    }
  }
}
resource "aws_iam_role" "rds_enhanced_monitoring" {
  # Provides an IAM role.

  # NOTE: If policies are attached to the role via the `aws_iam_policy_attachment` resource and you are modifying the
  # role `name` or `path`, the `force_detach_policies` argument must be set to `true` and applied before attempting the
  # operation otherwise you will encounter a `DeleteConflict` error.
  # The `aws_iam_role_policy_attachment` resource (recommended) does not have this requirement.
  count = local.enabled ? 1 : 0

  # (Optional, Forces new resource) Creates a unique name beginning with the specified prefix. Conflicts with `name`.
  name_prefix = "${var.rds_cluster_identifier}-rds-monitor-role-"
  # (Optional) The description of the role.
  description = "The RDS Cluster [${var.rds_cluster_identifier}] IAM role to allow enhanced metrics to be sent into CloudWatch."
  # Key-value mapping of tags for the IAM role.
  tags = merge(
    local.tags,
    {
      Name = "${var.rds_cluster_identifier}-rds-monitor-role"
    }
  )
  # (Required) The policy that grants an entity permission to assume the role.
  assume_role_policy = join("", data.aws_iam_policy_document.rds_assume_role.*.json)
}
resource "aws_iam_role_policy_attachment" "rds_enhanced_monitoring" {
  # Attaches a Managed IAM Policy to an IAM role.
  # NOTE(!): the usage of this resource conflicts with the aws_iam_policy_attachment resource and will permanently
  # show a difference if both are defined.
  count = local.enabled ? 1 : 0

  # (Required) The role the policy should be applied to.
  role = join("", aws_iam_role.rds_enhanced_monitoring.*.name)
  # (Required) The ARN of the policy you want to apply.
  policy_arn = "arn:aws:iam::aws:policy/service-role/AmazonRDSEnhancedMonitoringRole"
}


# Note: Requires random provider version >= 2.2.0
resource "random_password" "master_password" {
  # Identical to `random_string` resource with the exception that the result is treated as sensitive and, thus,
  # not displayed in console output.
  #   * https://www.terraform.io/docs/providers/random/r/password.html
  #   * https://www.terraform.io/docs/providers/random/r/string.html
  # Note: All attributes including the generated password will be stored in the raw state as plain-text. Read more:
  #   * https://www.terraform.io/docs/state/sensitive-data.html
  count = (local.enabled && length(var.rds_cluster_master_password) == 0) ? 1 : 0

  # (Required) The length of the string desired
  length = 40
  # (Optional) (default true) Include special characters in random string. These are !@#$%&*()-_=+[]{}<>:?
  special = false
  # (Optional) Arbitrary map of values that, when changed, will trigger a new id to be generated.
  keepers = {
    # hard-coded at the time
    password_version = 1
  }
}


# ----------------------
# RDS cluster resources
# ----------------------
resource "aws_rds_cluster_parameter_group" "cluster_parameter_group" {
  # Provides an RDS DB cluster parameter group resource. Documentation of the available parameters for various Aurora
  # engines can be found at:
  #   * Aurora MySQL Parameters - https://docs.aws.amazon.com/AmazonRDS/latest/UserGuide/AuroraMySQL.Reference.html
  #   * Aurora PostgreSQL Parameters - https://docs.aws.amazon.com/AmazonRDS/latest/UserGuide/AuroraPostgreSQL.Reference.html
  count = local.enabled ? 1 : 0

  # (Optional, Forces new resource) Creates a unique name beginning with the specified prefix. Conflicts with `name`.
  name_prefix = format("db-cluster-%s-parameter-group-", var.rds_cluster_identifier)
  # (Optional) The description of the DB cluster parameter group. Defaults to "Managed by Terraform".
  description = "Managed parameter group for the RDS [${var.rds_cluster_identifier}] Cluster."
  # (Optional) A mapping of tags to assign to the resource.
  tags = merge(
    local.tags,
    {
      Name = format("db-cluster-%s-parameter-group", var.rds_cluster_identifier)
    },
  )

  # (Required) The family of the DB cluster parameter group.
  family = var.rds_cluster_family

  dynamic "parameter" {
    for_each = var.rds_cluster_parameters

    content {
      apply_method = lookup(parameter.value, "apply_method", null)
      name         = parameter.value.name
      value        = parameter.value.value
    }
  }
}

resource "aws_db_parameter_group" "db_parameter_group" {
  # Provides an RDS DB parameter group resource. Documentation of the available parameters for various RDS engines
  # can be found at:
  #   * Aurora MySQL Parameters (https://docs.aws.amazon.com/AmazonRDS/latest/UserGuide/AuroraMySQL.Reference.html)
  #   * Aurora PostgreSQL Parameters (https://docs.aws.amazon.com/AmazonRDS/latest/UserGuide/AuroraPostgreSQL.Reference.html)
  #   * MariaDB Parameters (https://docs.aws.amazon.com/AmazonRDS/latest/UserGuide/Appendix.MariaDB.Parameters.html)
  #   * Oracle Parameters (https://docs.aws.amazon.com/AmazonRDS/latest/UserGuide/USER_ModifyInstance.Oracle.html#USER_ModifyInstance.Oracle.sqlnet)
  #   * PostgreSQL Parameters (https://docs.aws.amazon.com/AmazonRDS/latest/UserGuide/Appendix.PostgreSQL.CommonDBATasks.html#Appendix.PostgreSQL.CommonDBATasks.Parameters)
  count = local.enabled ? 1 : 0

  # (Optional, Forces new resource) Creates a unique name beginning with the specified prefix. Conflicts with `name`.
  name_prefix = format("db-instance-%s-parameter-group-", var.rds_cluster_identifier)
  # (Optional) The description of the DB parameter group. Defaults to "Managed by Terraform".
  description = "Managed parameter group for the particular DB instance from the RDS [${var.rds_cluster_identifier}] Cluster."
  # (Optional) A mapping of tags to assign to the resource.
  tags = merge(
    local.tags,
    {
      Name = format("db-instance-%s-parameter-group", var.rds_cluster_identifier)
    },
  )

  # (Required) The family of the DB parameter group.
  family = var.rds_cluster_family

  # (Optional) A list of DB parameters to apply. Note that parameters may differ from a family to an other. Full list
  # of all parameters can be discovered via `aws rds describe-db-parameters` after initial creation of the group.
  dynamic "parameter" {
    for_each = var.rds_cluster_instance_parameters

    content {
      apply_method = lookup(parameter.value, "apply_method", null)
      name         = parameter.value.name
      value        = parameter.value.value
    }
  }
}

resource "aws_rds_cluster" "cluster" {
  # Manages a RDS Aurora Cluster. To manage cluster instances that inherit configuration from the cluster (when not
  # running the cluster in "serverless" engine mode), see the `aws_rds_cluster_instance` resource.
  count = local.enabled ? 1 : 0

  # (Optional, Forces new resource) Creates a unique cluster identifier beginning with the specified prefix.
  # Conflicts with `cluster_identifier`.
  cluster_identifier_prefix = "${var.rds_cluster_identifier}-"
  # (Optional) A mapping of tags to assign to the DB cluster.
  tags = merge(
    local.tags,
    {
      Name = var.rds_cluster_identifier
    }
  )

  # (Optional) Name for an automatically created database on cluster creation.
  database_name = var.rds_cluster_db_name
  # (Optional) If the DB instance should have deletion protection enabled. The database can't be deleted when this
  # value is set to `true`.
  deletion_protection = var.rds_cluster_deletion_protection

  # (Required unless a snapshot_identifier or global_cluster_identifier is provided) Username for the master DB user.
  # Please refer to the RDS Naming Constraints. This argument does not support in-place updates and cannot be changed
  # during a restore from snapshot.
  master_username = var.rds_cluster_master_username
  # (Required unless a snapshot_identifier or global_cluster_identifier is provided) Password for the master DB user.
  # Note that this may show up in logs, and it will be stored in the state file. Please refer to the RDS Naming
  # Constraints
  master_password = length(var.rds_cluster_master_password) > 0 ? var.rds_cluster_master_password : join("", random_password.master_password.*.result)

  # (Optional) The name of your final DB snapshot when this DB cluster is deleted. If omitted, no final snapshot will
  # be made.
  final_snapshot_identifier = format("%s-final-snapshot", var.rds_cluster_identifier)
  # (Optional) Determines whether a final DB snapshot is created before the DB cluster is deleted. If true is
  # specified, no DB snapshot is created. If false is specified, a DB snapshot is created before the DB cluster is
  # deleted, using the value from final_snapshot_identifier. Default is false.
  skip_final_snapshot = var.rds_cluster_skip_final_snapshot

  # Optional) The target backtrack window, in seconds. Only available for aurora engine currently. To disable
  # backtracking, set this value to 0. Defaults to 0. Must be between 0 and 259200 (72 hours)
  backtrack_window = var.rds_cluster_backtrack_window
  # (Optional) The days to retain backups for. Default 1.
  backup_retention_period = var.rds_cluster_retention_period
  # (Optional) The daily time range during which automated backups are created if automated backups are enabled using
  # the BackupRetentionPeriod parameter.
  # Time in UTC Default: A 30-minute window selected at random from an 8-hour block of time per region.
  # e.g. 04:00-09:00
  preferred_backup_window = var.rds_cluster_preferred_backup_window
  # (Optional) The weekly time range during which system maintenance can occur, in (UTC) e.g. wed:04:00-wed:04:30
  preferred_maintenance_window = var.rds_cluster_preferred_maintenance_window

  # (Optional) List of VPC security groups to associate with the Cluster
  vpc_security_group_ids = [join("", aws_security_group.rds_cluster.*.id)]

  # (Optional) Specifies whether or not to create this cluster from a snapshot. You can use either the name or ARN when
  # specifying a DB cluster snapshot, or the ARN when specifying a DB snapshot.
  snapshot_identifier = var.rds_cluster_snapshot_identifier
  # (Optional) The global cluster identifier specified on aws_rds_global_cluster:
  #   * https://www.terraform.io/docs/providers/aws/r/rds_global_cluster.html
  global_cluster_identifier = var.rds_cluster_global_identifier
  # (Optional) ARN of a source DB cluster or DB instance if this DB cluster is to be created as a Read Replica.
  replication_source_identifier = var.rds_cluster_replication_source_identifier

  # (Optional) Specifies whether the DB cluster is encrypted. The default is `false` for `provisioned` `engine_mode`
  # and `true` for `serverless` `engine_mode`.
  storage_encrypted = var.rds_cluster_storage_encrypted
  # (Optional) The ARN for the KMS encryption key. When specifying kms_key_id, storage_encrypted needs to be set to
  # `true`.
  kms_key_id = var.rds_cluster_kms_key_id

  # (Optional) Specifies whether any cluster modifications are applied immediately, or during the next maintenance
  # window. Default is false. See Amazon RDS Documentation for more information^
  #   * https://docs.aws.amazon.com/AmazonRDS/latest/UserGuide/Overview.DBInstance.Modifying.html
  apply_immediately = var.rds_cluster_apply_immediately

  # (Optional) A DB subnet group to associate with this DB instance. NOTE: This must match the `db_subnet_group_name`
  # specified on every `aws_rds_cluster_instance` in the cluster.
  db_subnet_group_name = join("", aws_db_subnet_group.cluster_subnet_group.*.name)
  # (Optional) A cluster parameter group to associate with the cluster.
  db_cluster_parameter_group_name = join("", aws_rds_cluster_parameter_group.cluster_parameter_group.*.name)

  # (Optional) A List of ARNs for the IAM roles to associate to the RDS Cluster.
  iam_roles = var.rds_cluster_iam_roles
  # (Optional) Specifies whether or mappings of AWS Identity and Access Management (IAM) accounts to database accounts
  # is enabled. Please see AWS Documentation for availability and limitations^
  #   * https://docs.aws.amazon.com/AmazonRDS/latest/AuroraUserGuide/UsingWithRDS.IAMDBAuth.html
  iam_database_authentication_enabled = var.rds_cluster_iam_database_authentication_enabled

  # (Optional) The name of the database engine to be used for this DB cluster.
  # Defaults to `aurora`.
  # Valid values are:
  #   * aurora,
  #   * aurora-mysql,
  #   * aurora-postgresql
  engine = var.rds_cluster_engine
  # (Optional) The database engine mode.
  # Valid values are:
  #   * global,
  #   * multimaster,
  #   * parallelquery,
  #   * provisioned,
  #   * serverless.
  # Defaults to: provisioned. See the RDS User Guide for limitations when using serverless:
  # https://docs.aws.amazon.com/AmazonRDS/latest/UserGuide/aurora-serverless.html
  engine_mode = var.rds_cluster_engine_mode
  # (Optional) The database engine version.
  # NOTE: updating this argument results in an outage!!!
  # See the Aurora MySQL (https://docs.aws.amazon.com/AmazonRDS/latest/AuroraUserGuide/AuroraMySQL.Updates.html) and
  # Aurora Postgres (https://docs.aws.amazon.com/AmazonRDS/latest/AuroraUserGuide/AuroraPostgreSQL.Updates.html)
  # documentation for your configured engine to determine this value. For example with Aurora MySQL 2, a potential
  # value for this argument is `5.7.mysql_aurora.2.03.2`.
  engine_version = var.rds_cluster_engine_version

  # (Optional) The source region for an encrypted replica DB cluster.
  source_region = var.rds_cluster_source_region

  # (Optional) List of log types to export to cloudwatch. If omitted, no logs will be exported.
  # The following log types are supported:
  #   * audit,
  #   * error,
  #   * general,
  #   * slowquery,
  #   * postgresql (PostgreSQL).
  enabled_cloudwatch_logs_exports = var.rds_cluster_enabled_cloudwatch_logs_exports

  # (Optional) Nested attribute with scaling properties. Only valid when engine_mode is set to serverless.
  dynamic "scaling_configuration" {
    for_each = var.rds_cluster_scaling_configuration

    content {
      auto_pause               = lookup(scaling_configuration.value, "auto_pause", null)
      max_capacity             = lookup(scaling_configuration.value, "max_capacity", null)
      min_capacity             = lookup(scaling_configuration.value, "min_capacity", null)
      seconds_until_auto_pause = lookup(scaling_configuration.value, "seconds_until_auto_pause", null)
    }
  }
}

resource "aws_rds_cluster_instance" "cluster_instance" {
  # Provides an RDS Cluster Instance Resource. A Cluster Instance Resource defines attributes that are specific to a
  # single instance in a RDS Cluster, specifically running Amazon Aurora.
  count = local.enabled ? local.cluster_instance_count : 0

  # (Optional, Forces new resource) Creates a unique identifier beginning with the specified prefix.
  # Conflicts with `identifier`.
  identifier_prefix = format("%s-%s-", var.rds_cluster_identifier, count.index + 1)
  # (Required) The identifier of the `aws_rds_cluster` in which to launch this instance.
  cluster_identifier = join("", aws_rds_cluster.cluster.*.id)
  # (Optional) A mapping of tags to assign to the instance.
  tags = merge(
    local.tags,
    {
      Name = format("%s-%s", var.rds_cluster_identifier, count.index + 1)
    },
  )

  # (Optional) The name of the database engine to be used for the RDS instance.
  # Defaults to aurora.
  # Valid values are:
  #   * aurora,
  #   * aurora-mysql,
  #   * aurora-postgresql.
  # For information on the difference between the available Aurora MySQL engines see Comparison between Aurora MySQL 1
  # and Aurora MySQL 2 in the Amazon RDS User Guide (https://docs.aws.amazon.com/AmazonRDS/latest/UserGuide/AuroraMySQL.Updates.20180206.html).
  engine = var.rds_cluster_engine
  # (Optional) The database engine version.
  engine_version = var.rds_cluster_engine_version

  # (Required) The instance class to use. For details on CPU and memory, see Scaling Aurora DB Instances:
  #   * https://docs.aws.amazon.com/AmazonRDS/latest/UserGuide/Aurora.Managing.html
  # Aurora uses db.* instance classes/types.
  # Please see AWS Documentation (https://docs.aws.amazon.com/AmazonRDS/latest/UserGuide/Concepts.DBInstanceClass.html)
  # for currently available instance classes and complete details.
  instance_class = var.rds_cluster_instance_class

  # (Optional) Bool to control if instance is publicly accessible. Default false. See the documentation on
  # Creating DB Instances (https://docs.aws.amazon.com/AmazonRDS/latest/APIReference/API_CreateDBInstance.html) for
  # more details on controlling this property.
  publicly_accessible = var.rds_cluster_publicly_accessible
  # (Required if publicly_accessible = false, Optional otherwise) A DB subnet group to associate with this DB instance.
  # NOTE: This must match the `db_subnet_group_name` of the attached `aws_rds_cluster`.
  db_subnet_group_name = join("", aws_db_subnet_group.cluster_subnet_group.*.name)
  # (Optional) The name of the DB parameter group to associate with this instance.
  db_parameter_group_name = join("", aws_db_parameter_group.db_parameter_group.*.name)

  # (Optional) The ARN for the IAM role that permits RDS to send enhanced monitoring metrics to CloudWatch Logs.
  monitoring_role_arn = join("", aws_iam_role.rds_enhanced_monitoring.*.arn)
  # (Optional) The interval, in seconds, between points when Enhanced Monitoring metrics are collected for the DB
  # instance. To disable collecting Enhanced Monitoring metrics, specify 0.
  # The default is 0. Valid Values: 0, 1, 5, 10, 15, 30, 60.
  monitoring_interval = var.rds_cluster_monitoring_interval

  # (Optional, Computed) The EC2 Availability Zone that the DB instance is created in. See docs about the details.
  availability_zone = var.rds_cluster_availability_zone

  # (Optional) Specifies whether Performance Insights is enabled or not.
  performance_insights_enabled = var.rds_cluster_performance_insights_enabled
  # (Optional) The ARN for the KMS key to encrypt Performance Insights data. When specifying
  # `performance_insights_kms_key_id`, `performance_insights_enabled` needs to be set to `true`.
  performance_insights_kms_key_id = var.rds_cluster_performance_insights_kms_key_id
}

# ----------------------
# AutoScaling resources
# ----------------------
resource "aws_appautoscaling_target" "replicas" {
  # Provides an Application AutoScaling ScalableTarget resource:
  #   * https://docs.aws.amazon.com/autoscaling/application/APIReference/API_RegisterScalableTarget.html#API_RegisterScalableTarget_RequestParameters
  # To manage policies which get attached to the target, also see the `aws_appautoscaling_policy` resource.
  #   * https://www.terraform.io/docs/providers/aws/r/appautoscaling_target.html
  count = (local.enabled && var.rds_cluster_autoscaling_enabled) ? 1 : 0

  # (Required) The min capacity of the scalable target.
  min_capacity = var.rds_cluster_autoscaling_min_capacity
  # (Required) The max capacity of the scalable target.
  max_capacity = var.rds_cluster_autoscaling_max_capacity
  # (Required) The resource type and unique identifier string for the resource associated with the scaling policy.
  resource_id = "cluster:${join("", aws_rds_cluster.cluster.*.id)}"
  # (Required) The scalable dimension of the scalable target.
  scalable_dimension = "rds:cluster:ReadReplicaCount"
  # (Required) The AWS service namespace of the scalable target.
  service_namespace = "rds"
}

resource "aws_appautoscaling_policy" "replica_scaling_policy" {
  # Provides an Application AutoScaling Policy resource.
  count = (local.enabled && var.rds_cluster_autoscaling_enabled) ? 1 : 0

  # (Required) The name of the policy.
  name = format("%s-autoscaling-policy", var.rds_cluster_identifier)
  # (Optional) For DynamoDB, only TargetTrackingScaling is supported. For Amazon ECS, Spot Fleet, and Amazon RDS, both
  # StepScaling and TargetTrackingScaling are supported. For any other service, only StepScaling is supported.
  # Defaults to StepScaling.
  policy_type = var.rds_cluster_autoscaling_policy_type
  # (Required) The resource type and unique identifier string for the resource associated with the scaling policy.
  resource_id = join("", aws_appautoscaling_target.replicas.*.resource_id)

  # (Required) The scalable dimension of the scalable target.
  scalable_dimension = join("", aws_appautoscaling_target.replicas.*.scalable_dimension)
  # (Required) The AWS service namespace of the scalable target.
  service_namespace = join("", aws_appautoscaling_target.replicas.*.service_namespace)

  # (Optional) A target tracking policy, requires policy_type = "TargetTrackingScaling".
  target_tracking_scaling_policy_configuration {
    # (Required) The target value for the metric.
    target_value = var.rds_cluster_autoscaling_target_value
    # (Optional) Indicates whether scale in by the target tracking policy is disabled. If the value is `true`,
    # scale in is disabled and the target tracking policy won't remove capacity from the scalable resource. Otherwise,
    # scale in is enabled and the target tracking policy can remove capacity from the scalable resource.
    # The default value is `false`.
    disable_scale_in = false
    # (Optional) The amount of time, in seconds, after a scale in activity completes before another scale in activity
    # can start.
    scale_in_cooldown = var.rds_cluster_autoscaling_scale_in_cooldown
    # (Optional) The amount of time, in seconds, after a scale out activity completes before another scale out activity
    # can start.
    scale_out_cooldown = var.rds_cluster_autoscaling_scale_out_cooldown

    # (Optional) A predefined metric.
    predefined_metric_specification {
      # (Required) The metric type.
      predefined_metric_type = var.rds_cluster_autoscaling_predefined_metric_type
    }
  }
}
