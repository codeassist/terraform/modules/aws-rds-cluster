terraform {
  required_version = "~> 0.12"

  required_providers {
    aws    = "~> 2.60"
    random = "~> 2.2"
  }
}
