output "cluster_arn" {
  description = "Amazon Resource Name (ARN) of cluster."
  value       = local.enabled ? join("", aws_rds_cluster.cluster.*.arn) : ""
}
output "cluster_id" {
  description = "The RDS Cluster Identifier."
  value       = local.enabled ? join("", aws_rds_cluster.cluster.*.id) : ""
}
output "cluster_identifier" {
  description = "Cluster Identifier."
  value       = local.enabled ? join("", aws_rds_cluster.cluster.*.cluster_identifier) : ""
}
output "cluster_resource_id" {
  description = "The region-unique, immutable identifie of the cluster"
  value       = local.enabled ? join("", aws_rds_cluster.cluster.*.cluster_resource_id) : ""
}

output "cluster_members" {
  description = "List of RDS Instances that are a part of this cluster."
  value       = local.enabled ? flatten(coalescelist(aws_rds_cluster.cluster.*.cluster_members, [""])) : []
}

output "cluster_endpoint" {
  description = "The DNS address of the RDS instance."
  value       = local.enabled ? join("", aws_rds_cluster.cluster.*.endpoint) : ""
}
output "reader_endpoint" {
  description = "A read-only endpoint for the Aurora cluster, automatically load-balanced across replicas."
  value       = local.enabled ? join("", aws_rds_cluster.cluster.*.reader_endpoint) : ""
}

output "engine" {
  description = "The database engine."
  value       = local.enabled ? join("", aws_rds_cluster.cluster.*.engine) : ""
}
output "engine_version" {
  description = "The database engine version."
  value       = local.enabled ? join("", aws_rds_cluster.cluster.*.engine_version) : ""
}
output "database_name" {
  description = "The database name."
  value       = local.enabled ? join("", aws_rds_cluster.cluster.*.database_name) : ""
}
output "port" {
  description = "The database port."
  value       = local.enabled ? join("", aws_rds_cluster.cluster.*.port) : ""
}

output "master_username" {
  description = "Username for the master DB user"
  value       = local.enabled ? join("", aws_rds_cluster.cluster.*.master_username) : ""
}
output "master_password" {
  description = "Username for the master DB user"
  value       = local.enabled ? (length(var.rds_cluster_master_password) > 0 ? var.rds_cluster_master_password : join("", random_password.master_password.*.result)) : ""
  sensitive   = true
}

output "cluster_security_groups" {
  description = "Default RDS cluster security groups."
  value       = local.enabled ? flatten(coalescelist(aws_rds_cluster.cluster.*.vpc_security_group_ids, [""])) : []
}

output "dbi_resource_ids" {
  description = "List of the region-unique, immutable identifiers for the DB instances in the cluster."
  value       = local.enabled ? aws_rds_cluster_instance.cluster_instance.*.dbi_resource_id : []
}
